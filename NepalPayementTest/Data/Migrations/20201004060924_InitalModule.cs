﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NepalPayementTest.Data.Migrations
{
    public partial class InitalModule : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserTask",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TaskName = table.Column<string>(nullable: true),
                    AssignDate = table.Column<DateTime>(nullable: false),
                    DeadlineDead = table.Column<DateTime>(nullable: false),
                    taskType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTask", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserTask");
        }
    }
}
