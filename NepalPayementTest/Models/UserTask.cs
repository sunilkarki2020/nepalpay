﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NepalPayementTest.Models
{
    public class UserTask
    {
        public Guid Id { get; set; }
        public string TaskName { get; set; }
        public DateTime AssignDate { get; set; }
        public DateTime DeadlineDead { get; set; }
        public TaskType TaskType { get; set; }

    }
}
